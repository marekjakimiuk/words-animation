'use strict'

class wordsAnimation {
  constructor (params) {
    const paramsDefault = {
      selector: '*[words-animation]',
      offset: 400,
      delay: 100,
      mode: 'word',
      activeClass: 'is-active'
    }
    const paramsProp = paramsDefault
    if (typeof params === `object` && (params !== undefined || 'undefined')) {
      Object.keys(params).map(elem => {
        paramsProp[elem] = params[elem]
      })
      this.params = paramsProp
    } else if (typeof params === 'string') {
      console.error('WordsAnimation : Param should be an object.');
    } else {
      console.table('test', [])
      this.params = paramsDefault
    }
  }

  changeContent (mode, list) {
    const activeClass = this.params.activeClass
    list.innerHTML = list.innerHTML.split(mode).reduce((prev, word) => {
      prev.push(`<span class="anim">${word}</span>`)
      return prev
    }, []).join(mode)
    list.querySelectorAll('.anim').forEach((char, index) => {
      setTimeout(function () {
        char.classList.add(activeClass)
      }, index * this.params.delay, activeClass)
    })
  }

  init () {
    const paramEl = document.querySelectorAll(this.params.selector);
    if (paramEl.length > 0) {
      paramEl.forEach(list => {
        const mode = list.getAttribute('wa-mode') || 'word'
        this.params.delay = parseInt(list.getAttribute('wa-delay')) || parseInt(this.params.delay)
        switch (mode) {
          case 'char':
            this.changeContent('', list)
            break
          case 'word':
            this.changeContent(' ', list)
            break
        }
      })
    }
  }
}
