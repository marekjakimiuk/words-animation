// var path = require('path')
// var webpack = require('webpack')
// module.exports = {
//   entry: './src/app.js',
//   output: {
//     path: path.resolve(__dirname, 'build'),
//     filename: 'app.min.js'
//   },
//   module: {
//     loaders: [
//       {
//         test: /\.js$/,
//         loader: 'babel-loader',
//         query: {
//           presets: ['es2015']
//         }
//       }
//     ]
//   },
//   stats: {
//     colors: true
//   },
//   devtool: 'source-map'
// }

var path = require('path');

module.exports = {
  entry: './src/app.js',
  resolve: {
    extensions: ['.webpack.js', '.web.js', '.ts', '.js']
  },
  module: {
    rules: [
      { test: /.js$/, loader: 'babel-loader' }
    ]
  },
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist')
  }
}
